from Location import Location
import Consts

class Hero(object):
    def __init__(self, game, status, locationName, locationGS, lastAnswer, var, items):
        self.game =  game
        self.lastStatus = status
        self.location = Location(locationName, game, locationGS)
        self.lastAnswer = lastAnswer
        self.var = var
        self.items = items
        if(len(self.var) == 0):
            self.autoconfigVars()

    def autoconfigVars(self):
        self.var["tootToDeliver"] = Consts.tootToDeliverBase
        self.var["username"] = self.getUserName()
        self.var["instance"] = Consts.gameInstanceName
        self.var["prenom"] = Consts.gamePrenom
        self.var["nom"] = Consts.gameNom

    def getPossibleCommands(self):
        return self.location.possibleCommands

    def getPossibleParams(self, index):
        return self.location.possibleParams[index]

    def getUserName(self):
        return self.lastStatus["account"]["username"]

    def getUserID(self):
        return self.lastStatus["account"]["id"]

    def getAccountName(self):
        return self.lastStatus["account"]["acct"]

    def getVar(self, varName, defaultValue = 0):
        return self.var.setdefault(varName, defaultValue)

    def executeCommand(self, command, params):
        self.location.run(command, params)


    def teleport(self, locName, globalScripts = Consts.gameGlobalScript):
        self.location = Location(locName, self.game, globalScripts)
        self.location.atEnter()

    def addItem(self, item, qty = 1):
        if(item.internalName in self.items):
            self.items[item.internalName][1] += qty #qty can be negative !
            if(self.items[item.internalName][1] == 0): #there is a quantity of 0 item -> we remove it
                del self.items[item.internalName]
        else:
            if(qty > 0):
                self.items[item.internalName] = [item, qty]


    def getItemQty(self, item):
        if(item.internalName in self.items):
            return self.items[item.internalName][1]
        else:
            return 0

    def savePart(self):
        return [self.location.name, self.location.globalScriptsName, self.lastAnswer, self.var, self.items]

    def reset(self):
        self.var = {}
        self.items = {}
        self.autoconfigVars()
        self.teleport(Consts.firstScene)
