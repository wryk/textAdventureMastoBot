import Consts
import time

def logprint(text, verbose = Consts.verboseBase):
    text = time.asctime( time.localtime(time.time()) ) + " : " + text + "\n\n"
    if(verbose >= Consts.verboseError):
        print(text)
    with open(Consts.log, 'a') as output:
        output.write(text)

def debugprint(text):
    text = text + "\n"
    print(text)
    with open(Consts.debugOutput, 'a') as output:
        output.write(text)
