import mastodon
import os
import time


from TootHTMLParser import TootHTMLParser
from MastoTextGame import MastoTextGame
import Consts
import Log

#a Mastodon Stream Listener  defines functions to react when something happens on Mastodon. We inherit it.
class BotListener(mastodon.StreamListener):
    def __init__(self, bot):
        self.bot = bot
        self.botName = self.bot.botName

    #What happens if the bot receives a notification ? We only want to react to DM.
    def on_notification(self, notification):
        self.bot.sinceID = notification["id"]
        open(Consts.sinceID, 'w').write(str(self.bot.sinceID))
        if(notification['type'] != 'mention'):
            return

        status = notification['status']
        if(status['visibility'] != 'direct'):
            return

        content = str(status["content"])
        parser = TootHTMLParser()
        parser.feed(content)
        content = parser.txt
        if(not content.startswith(self.botName)): # We ignore status where we aren't directly mentionned
            return

        Log.logprint("DM received. I will react to this.")

        #So we know that we've just received a DM.
        #We'll find the hero (and corresponding data) associated with the author of the DM ;
        # and find what the hero wants to do : is there a command in the toot ? If found, we execute the command.
        # This we'll probably modify the story for the player, so we save the info we have about the hero.
        self.bot.game.reactToStatus(status, content)

        #We just set a sleep to avoid spamming ? No idea what I am doing here :shrug:
        time.sleep(1)


#The bot that rocks. Can listen to events and publish toots !
class Bot(object):
    def __init__(self):
        #First we can connect using the api.
        api_base_url = open(Consts.apiBaseURLFile).read()
        self.apiMasto = mastodon.Mastodon(client_id = Consts.appCred, access_token = Consts.accountCred,api_base_url = api_base_url)
        Log.logprint("Bot connected.")

        #We'll treat every message since the last one we treated.
        if os.path.exists(Consts.sinceID) and open(Consts.sinceID).read().isdigit():
            self.sinceID = int(open(Consts.sinceID).read())
        else:
            self.sinceID = 0

        #Name of the bot account
        self.botName = '@' + open(Consts.botNameFile).read()

        #The bot's purpose is to run a game !
        self.game = MastoTextGame(self)
        Log.logprint("Game created.")

    def publishDebug(self, text, updateLastAnswer):
        hero = self.game.hero
        atUser = '@debugMode@debug.town'
        if(updateLastAnswer):
            self.game.hero.lastAnswer = text

        text = text.split(Consts.tootSeparator)
        text = [atUser + ' ' + t for t in text]
        if(len(atUser) <= Consts.nameMaxLength):
            for t in text:
                while(len(t) > Consts.tootLength):
                    Log.debugprint(t[:Consts.tootLength-1])
                    t = atUser + ' ' + t[Consts.tootLength-1:]

                status = Log.debugprint(t[:Consts.tootLength-1])


    #We'll toot the text in reply to the status.
    def publish(self, text, updateLastAnswer = True):
        if(Consts.debug):
            return self.publishDebug(text, updateLastAnswer)
        #We save what the hero has seen last (this text!)
        hero = self.game.hero
        status = hero.lastStatus
        atUser =  '@' + status['account']['acct']
        if(updateLastAnswer):
            self.game.hero.lastAnswer = text

        #If text is too long for a toot, we'll split it into several toots.
        text = text.split(Consts.tootSeparator)
        text = [atUser + ' ' + t for t in text]
        #Yeah, we don't answer people with a username which is too long. Sorry I don't make the rules :shrug:
        if(len(atUser) <= Consts.nameMaxLength):
            for t in text:
                #Last modifications in the text : we add media if we find \media[path/to/media.ext] or we create CWs \CW[nourriture]
                medias = []
                CW = None

                words = t.split("\\") #we separate  by words

                for i in range(len(words)):
                    if (len(medias) < 4 and words[i][:6] == "media["):
                        index = words[i].find("]")
                        if(index > 6):
                            mediaName = Consts.mediaDirectory + words[i][6:index]  #extraction of name in \media[name]
                            if(os.path.exists(mediaName)):
                                medias.append(self.apiMasto.media_post(mediaName))
                            words[i] = words[i][index+1:] #we delete the media command

                    elif(words[i][:3] == "CW["):
                        index = words[i].find("]")
                        if(index > 3):
                            CW = words[i][3:index]
                            words[i] = words[i][index+1:]

                if(len(medias) > 0 or CW is not None): #we modified the text by removing a media command so we reconstruct the text without them
                    t = ""
                    for word in words:
                        t += word


                while(len(t) > Consts.tootLength):
                    status = self.apiMasto.status_post(t[:Consts.tootLength-1], in_reply_to_id=status['id'], visibility='direct')
                    t = atUser + ' ' + t[Consts.tootLength-1:]


                status = self.apiMasto.status_post(t[:Consts.tootLength-1], in_reply_to_id=status['id'], visibility='direct', media_ids=tuple(medias), spoiler_text = CW)

    #We just stream the notifications. Never ending function (in theory)
    def listen(self):
        listener = BotListener(self)
        Log.logprint("Listening for events since last time (ID " + str(self.sinceID) + ")")
        for notification in reversed(self.apiMasto.notifications(since_id=self.sinceID)):
            listener.on_notification(notification)


        Log.logprint("Now listening for new events...")
        self.apiMasto.stream_user(listener)

    def emergencyContact(self):
        contact = open(Consts.emergencyContactFile).read()
        self.apiMasto.status_post('@' + contact + " " + Consts.emergencyMessage, visibility = 'direct')
