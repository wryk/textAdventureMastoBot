import  pickle
import random


import Consts
import Log

#The Game ! Linked to the bot forever. <3
#It makes the link between the user and a hero in the game.
#At each status we update the current hero
class GeneralGame(object):
    def __init__(self, bot):
        self.bot = bot
        self.hero = None
        #We set the random seed
        random.seed()

    def reactToStatus(self, status, content):
        self.associateUserToHero(status) #self.hero = ...
        Log.logprint("Hero associated to status. Now I'll interpret the status.")
        self.interpretStatusContent(content.lower())

        self.save()
        Log.logprint("Done.")

    def interpretStatusContent(self, textBase):
        if(self.hero is None):
            return

        text = textBase.split(" ")
        # (1) load the possible commands in the current scene.
        commands = self.hero.getPossibleCommands()
        commandFound = ""
        indexCommand = -1
        if(len(commands) > 0):
            for i in range(len(commands)):
                for j in range(len(commands[i])):
                    if(commands[i][j] in text):
                        commandFound = commands[i][0]
                        indexCommand = i
                        break
                if(indexCommand >= 0):
                    break
        if(indexCommand >= 0):
            paramsCommand = self.hero.getPossibleParams(indexCommand) #Parameters for this command
            paramsFound = ["" for j in range(len(paramsCommand))] #see if there is an error when len is 0 ;
            if(len(paramsCommand) > 0):
                for j in range(len(paramsCommand)):
                    for param in paramsCommand[j]:
                        for synonym in param:
                            if(synonym in text):
                                paramsFound[j] = param[0]
                                break
                            elif(synonym == Consts.everythingIsTheParameter): #special
                                paramsFound[j] = textBase
        # (2) if something found in the text; try to interpret !
        #interpret the command with the params is the job of the location.
            self.hero.executeCommand(commandFound, paramsFound)
            return
        # (3) if nothing : give a message of "I don't understand, maybe you want help ?"
        else: #
            self.notUnderstood()
            return


    def notUnderstood(self):
        toPublish = Consts.notUnderstood[random.randrange(len(Consts.notUnderstood))]
        toPublish += "\n\n" + Consts.inviteToHelp
        self.bot.publish(toPublish, False)

    def publish(self, text, updateLastAnswer = True):
        self.bot.publish(text, updateLastAnswer)

    def retrieveText(self, filename):
        f = open(Consts.textsDirectory + filename, "r")
        return self.customizeText(f.read())

    def customizeText(self, text):
        return text

    def associateUserToHero(self, status):
        return None

    def save(self):
        if(self.hero is not None):
            self.save_object(self.hero.savePart(), Consts.heroesDirectory + self.hero.getAccountName()+ ".txt")


    def save_object(self, obj, filename):
        with open(filename, 'wb') as output:  # Overwrites any existing file.
            pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)
    def load_object(self, filename):
        with open(filename, 'rb') as input:
            return pickle.load(input)
