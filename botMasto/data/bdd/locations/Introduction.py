import Consts
import Log
from Item import Item

def loadCommands():
    return [Consts.actionLookCmd, Consts.actionWaitCmd, Consts.actionListenCmd,
    Consts.actionGoCmd, Consts.actionTakeCmd, Consts.actionUseCmd,
    Consts.actionLeaveCmd, Consts.actionWearCmd, Consts.actionEatCmd,
    Consts.actionDrink, ["faire"], Consts.actionPissCmd,
    Consts.actionPoopCmd, Consts.actionDanceCmd, Consts.actionSleepCmd,
    ["assembler"], ["monter"], Consts.actionOpenCmd, Consts.actionPatCmd]


balai = ["balai"]
manche = ["manche"]
tete = ["tête", "tete"]
coffre = ["coffre", "malle"]
toot = ["toot"]
tuyau = ["tuyau", "tuyaux"]
tootomatic = ["tootomatic"]
porte = ["porte"]
dehors = ["dehors", "extérieur", "exterieur", "l'extérieur", "l'exterieur"]
atelier = ["atelier", "autour", "l'atelier"]
armoire = ["armoire", "l'armoire"]
harmoniseur = ["harmoniseur", "l'harmoniseur"]
frigo = ["froidimension","frigo", "frigidaire", "réfrigérateur", "refrigerateur"]
cupcake = ["cupcake", "cupcakes", "cake", "cakes", "gateau", "gâteau", "gâteaux", "gateaux"]
bouteille = ["bouteille"]
toilettes = ["toilettes", "toilette", "chiotte", "chiottes", "WC"]
lunettes = ["lunettes"]
pipicaca = ["pipi", "caca", "crotte"]
sieste = ["sieste"]

regarder = [[balai, manche, tete, coffre, toot, tuyau, tootomatic, porte, dehors, atelier, armoire, harmoniseur, frigo, cupcake, bouteille, toilettes, lunettes]]
attendre = [[]]
ecouter = [[tootomatic, harmoniseur, dehors, frigo]]
aller = [[dehors, toilettes, frigo]]
prendre = [[manche, tete, toot, cupcake, bouteille]]
utiliser = [[balai, tootomatic, harmoniseur, frigo, toilettes]]
partir = [[dehors]]
porter = [[lunettes]]
manger = [[cupcake]]
boire = [[bouteille]]
faire = [[pipicaca, sieste]]
uriner = [[]]
defequer = [[]]
danser = [[]]
dormir = [[]]
assembler = [[balai]]
monter = [[balai]]
ouvrir = [[coffre, porte, armoire, frigo]]
pat = [[]] #pandabot dedans, par défaut ?

def loadParams():
    return [regarder, attendre, ecouter, aller, prendre, utiliser, partir, \
    porter, manger, boire, faire, uriner, defequer, danser, dormir, assembler,\
    monter, ouvrir, pat]


textEnter = """Ça, c'est moi. \media[WitchSelfie.png]

Je suis une sorcière de l'instance Mastodon autogérée witches.camp.

Enfin... Je viens d'arriver, il faut que je prenne mes marques encore, évidemment.

Comme c'est autogéré, on fait tout nous-même ! On s'organise. C'est assez excitant, même si parfois c'est un peu déroutant.
Aujourd'hui, pour la première fois je m'occupe de la livraison de toots.
\\tootSeparator
C'est pour ça que je suis à l'atelier. Mais... Ça fait 25 minutes que j'y suis et il n'y a toujours personne...

Normalement une sorcière aguerrie devait me guider mais elle n'est pas là.

...


...

Du coup... Je ne sais pas trop quoi faire. Enfin, si en théorie c'est assez simple : je prends mon balai, je prends tous les toots à livrer et je suis les panneaux !

Mais bon, une petite initiation n'aurait pas été de refus ! ^^\"
\\tootSeparator
...

...

J'en peux plus d'atteeeeeendre. Forcément pour ma première fois en plus.

Faut que je m'occupe je ne peux pas faire qu'attendre. Je pourrais... regarder les toots à livrer ? Ils sont dans le coffre à toots de l'atelier.
"""
def atEnter(game):
    game.hero.var["Introduction.Attendre"] = 0
    game.hero.var["Introduction.ArmoireOuverte"] = False
    game.hero.var["Introduction.CoffreOuvert"] = False
    game.hero.var["Introduction.ManchePris"] = False
    game.hero.var["Introduction.tetePrise"] = False
    game.hero.var["Introduction.harmoniseurUsed"] = False
    game.hero.var["Introduction.lunettesPort"] = False
    game.hero.var["Introduction.tootPris"] = False
    game.hero.var["Introduction.CupcakePris"] = False
    game.hero.var["Introduction.BouteillePrise"] = False
    game.hero.addItem(Item.get("lunettes"))
    game.bot.publish(textEnter)


atelierDesc = """L'atelier. Une pièce assez grande, à la base elle était pour la réparation des balais mais finalement on a mis le tootomatic et le coffre à toots à livrer ici aussi. En plus il y a une armoire où on stocke les balais. L'harmoniseur est là pour assembler les balais. On a aussi prévu de quoi combler un petit creux avec la froidimension, et à l'arrière se trouvent les toilettes de l'atelier. En face il y a la porte par laquelle je suis entrée."""
def run(game, command, args):
    if(command == Consts.actionLook):
        if(args[0] == balai[0]):
            if(not game.hero.var["Introduction.ArmoireOuverte"]):
                game.publish("Bien sur, il faudrait que je me trouve un balai...")
            else:
                if(not game.hero.var["Introduction.harmoniseurUsed"]):
                    game.publish("J'ai bien pu trouver de quoi faire un balai mais il faudrait que je l'assemble sinon je ne vais rien pouvoir faire avec.")
                else:
                    txt= game.customizeText("J'ai à ma disposition un joli balai fin prêt à être utilisé ! C'est pas n'importe quel balai \\var[prenom], c'est un Cleanus 2000 !")
                    game.publish(txt)

        elif(args[0] == manche[0]):
            if(not game.hero.var["Introduction.ArmoireOuverte"] or game.hero.var["Introduction.harmoniseurUsed"]):
                game.notUnderstood()
            else:
                game.publish("Le manche du balai. Long, fin, rectiligne, en vieux chêne, il assure la stabilité dans le couple manche-tête. Il me permet aussi de me diriger quand je suis sur le balai, ce qui n'est pas mal.")

        elif(args[0] == tete[0]):
            if(not game.hero.var["Introduction.ArmoireOuverte"] or game.hero.var["Introduction.harmoniseurUsed"]):
                game.notUnderstood()
            else:
                game.publish("La tête du balai. Un faisceau de branchettes hautement magiques : un concentré d'énergie instable qui permet de propulser les balais pour peu qu'on arrive à le contrôler.")

        elif(args[0] == coffre[0]):
            if(not game.hero.var["Introduction.CoffreOuvert"]):
                game.publish("Le coffre à toot. Un authentique coffre de pirate il parait. Enfin à une époque. Il est relié par des tuyaux au tootomatic qui achemine les toots dans le coffre. Et tous les matins, on va livrer les toots.")
            else:
                if(not game.hero.var["Introduction.tootPris"]):
                    game.publish("Le... Le coffre ne contient qu'un seul toot ! Ça c'est la meilleure... L'instance n'a pas été très active il faut croire.")
                else:
                    game.publish("Le coffre à toot est vide à présent.")

        elif(args[0] == toot[0]):
            if(not game.hero.var["Introduction.CoffreOuvert"]):
                game.notUnderstood()
            else:
                if(not game.hero.var["Introduction.tootPris"]):
                    game.publish(game.customizeText("Voilà donc le seul et unique toot à délivrer aujourd'hui. Je devrais le prendre, après tout il va bien falloir le livrer... L'adresse du destinataire au dos est Gargron@mastodon.social.  \\tootToDeliver"))
                else:
                    game.publish("*un bruit retentissant se fait entendre*\n\n Maintenant que j'ai pris le toot, il est dans mon inventaire. Je peux l'examiner en passant par celui-ci : " + Consts.CommandInventory[0] + " toot \n\n (le bruit semble être celui du 4e mur qui vient d'être cassé. Oupsi.)")

        elif(args[0] == tuyau[0]):
            game.publish(game.customizeText(" \\tootToDeliver  Un complexe réseau de tuyaux relie le coffre au tootomatic, et le tootomatic à chaque compte des sorcières de witches.camp. Comme ça on a plus qu'à livrer les nouveaux toots !"))

        elif(args[0] == tootomatic[0]):
            game.publish("Le tootomatic c'est la grosse machine qui permet aux toots à envoyer de se retrouver à l'atelier. Elle est entièrement faite en matériaux de récup' mais elle fonctionne très bien, avec un peu d'entretien.")

        elif(args[0] == porte[0]):
            game.publish("Et bien en voilà une belle porte.")

        elif(args[0] == dehors[0]):
            game.publish("J'admirerai l'extérieur une fois sortie de l'atelier.")

        elif(args[0] == atelier[0]):
            game.publish(atelierDesc)

        elif(args[0] == armoire[0]):
            game.publish("L'armoire sert à stocker les balais de livraison quand on ne s'en sert pas, ou les balais à réparer s'il y en a.")

        elif(args[0] == harmoniseur[0]):
            game.publish("L'harmoniseur, c'est la machine qui sert à assembler les manches et les têtes de balai. \nCes deux parties sont d'une nature magique très différente et ne s'assemblent pas naturellement. L'harmoniseur permet de caler les ondes magiques des deux objets pour qu'ils coopèrent et forment le balai, qui est plus que la somme de ses parties. Bref, de la magie de base.\n\n Il est écrit \"PORT DE LUNETTES OBLIGATOIRE\"")

        elif(args[0] == frigo[0]):
            game.publish("La froidimension, c'est un portail vers une dimension qui est toujours à la bonne température pour conserver des aliments. C'est pratique, écolo, et ça ne prend pas de place.")

        elif(args[0] == cupcake[0]):
            if(not game.hero.var["Introduction.CupcakePris"]):
                game.publish("Il reste un joli cupcake dans la froidimension. Au chocolat, je crois.")
            else:
                game.publish("*un bruit retentissant se fait entendre*\n\n Maintenant que j'ai pris le cupcake, il est dans mon inventaire. Je peux l'examiner en passant par celui-ci : " + Consts.CommandInventory[0] + " cupcake \n\n (le bruit semble être celui du 4e mur qui vient d'être cassé. Oupsi.)")

        elif(args[0] == bouteille[0]):
            if(not game.hero.var["Introduction.BouteillePrise"]):
                game.publish("Il reste une bouteille d'eau fraiche dans la froidimension.")
            else:
                game.publish("*un bruit retentissant se fait entendre*\n\n Maintenant que j'ai pris la bouteille, elle est dans mon inventaire. Je peux l'examiner en passant par celui-ci : " + Consts.CommandInventory[0] + " bouteille \n\n (le bruit semble être celui du 4e mur qui vient d'être cassé. Oupsi.)")

        elif(args[0] == toilettes[0]):
            game.publish("Et bien oui, même les sorcières vont aux toilettes.")

        elif(args[0] == lunettes[0]):
            game.publish("*un bruit retentissant se fait entendre*\n\n Les lunettes sont dans mon inventaire. Je peux l'examiner en passant par celui-ci : " + Consts.CommandInventory[0] + " lunettes \n\n (le bruit semble être celui du 4e mur qui vient d'être cassé. Oupsi.)")

        else: #nothing found
            game.publish(atelierDesc)


    elif(command == Consts.actionWait):
        if(game.hero.var["Introduction.Attendre"] < 5):
            game.publish("tou pi dou pi dou wa... \n \n ... \n \n .......... \n \nToujours personne. Je veux faire quelque chose !")
            game.hero.var["Introduction.Attendre"] = game.hero.var["Introduction.Attendre"] + 1
        else:
            game.publish("MAIS J'EN PEUX PLUS D'ATTENDRE AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHHH")


    elif(command == Consts.actionListen):
        if(args[0] == tootomatic[0]):
            game.publish("Le tootomatic ronronne doucement. Malgré la complexité de cette machine, elle est plutôt silencieuse finalement. On entend parfois la vapeur sortir quand il y en a trop.")

        elif(args[0] == harmoniseur[0]):
            game.publish("Au moins quand on n'utilise pas l'harmoniseur, il ne fait pas de bruit... \n\nPar contre, une fois qu'on lui donne un balai à monter l'harmoniseur produit un vacarme assez caractéristique.")

        elif(args[0] == dehors[0]):
            game.publish("Je n'entend personne arriver.")

        elif(args[0] == frigo[0]):
            game.publish("La froidimension émet une onde sonore pure.")

        else:
            game.publish("Tout est calme ici... Pas un bruit dehors, et ici, juste les petits bruits habituels du tootomatic et de la froidimension.")


    elif(command == Consts.actionGo):
        if(args[0] == dehors[0]):
            if(game.hero.var["Introduction.harmoniseurUsed"]):
                if(game.hero.var["Introduction.tootPris"]):
                    game.publish("Donc... \n\nJ'ai mon balai... \nJ'ai le toot à délivrer... \nToujours personne... \n\n Plus qu'à y aller !!")
                    game.hero.teleport("Outside")
                else:
                    game.publish("J'ai mon balai... Je pourrais délivrer le toot. Il faut que je le prenne !")
            else:
                game.publish("Je ne vais quand même pas sortir sans balai !")

        elif(args[0] == toilettes[0]):
            game.publish("C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente.")

        elif(args[0] == frigo[0]):
            game.publish("L'ouverture de la froidimension est trop petite pour que je puisse rentrer dedans... Et heureusement, je crois.")

        else:
            game.publish("Hein ? Aller où ?")


    elif(command == Consts.actionTake):
        if(args[0] == manche[0]):
            if(game.hero.var["Introduction.ArmoireOuverte"]):
                game.publish("J'ai le manche du balai avec moi.")
                game.hero.var["Introduction.ManchePris"] = True
                game.hero.addItem(Item.get("mancheBalai"))
            else:
                game.notUnderstood()

        elif(args[0] == tete[0]):
            if(game.hero.var["Introduction.ArmoireOuverte"]):
                game.publish("J'ai la tête du balai avec moi.")
                game.hero.var["Introduction.tetePrise"] = True
                game.hero.addItem(Item.get("têteBalai"))

            else:
                game.notUnderstood()

        elif(args[0] == toot[0]):
            if(game.hero.var["Introduction.CoffreOuvert"]):
                game.publish(game.customizeText("Voilà donc le seul et unique toot à délivrer aujourd'hui. Je le prends, après tout il va bien falloir le livrer... L'adresse du destinataire au dos est Gargron@mastodon.social.  \\tootToDeliver"))
                game.hero.var["Introduction.tootPris"] = True
                game.hero.addItem(Item.get("tootToDeliver"))
            else:
                game.notUnderstood()

        elif(args[0] == cupcake[0]):
            if(not game.hero.var["Introduction.CupcakePris"]):
                game.hero.var["Introduction.CupcakePris"] = True
                game.hero.addItem(Item.get("cupcake"))
                game.publish("Je prends ce joli cupcake avec moi... Après tout, je sais que j'en ferai bon usage.")
            else:
                game.publish("J'ai déjà pris le cupcake, il n'y en a plus malheureusement...")

        elif(args[0] == bouteille[0]):
            if(not game.hero.var["Introduction.BouteillePrise"]):
                game.hero.var["Introduction.BouteillePrise"] = True
                game.hero.addItem(Item.get("bouteille"))
                game.publish("Je prends la bouteille avec moi... C'est important d'être hydratée.")
            else:
                game.publish("J'ai déjà pris la bouteille, il n'y en a plus.")


        else:
            game.notUnderstood()

    elif(command == Consts.actionUse):
        if(args[0] == balai[0]):
            if(game.hero.var["Introduction.harmoniseurUsed"]):
                game.publish("Mmmh... Avant d'utiliser mon balai je devrais surement sortir, ça éviterait des accidents.")
            elif(game.hero.var["Introduction.ArmoireOuverte"]):
                game.publish("Avant de pouvoir utiliser mon balai je devrais l'assembler.")
            else:
                game.publish("Mmmh... Il faudrait que je me trouve un balai.")

        elif(args[0] == tootomatic[0]):
            game.publish("Je ne peux pas vraiment utiliser le tootomatic... Il fonctionne tout seul en récupérant les toots de witches.camp pour les rassembler dans le coffre à toots.")

        elif(args[0] == harmoniseur[0]):
            if(game.hero.var["Introduction.harmoniseurUsed"]):
                game.publish("J'ai déjà utilisé l'harmoniseur, j'ai mon balai désormais.")
            else:
                if(not game.hero.var["Introduction.lunettesPort"]):
                    game.publish("Safety first. Je devrais mettre mes lunettes.")
                else:
                    if(game.hero.var["Introduction.tetePrise"]):
                        if(game.hero.var["Introduction.ManchePris"]):
                            game.publish("Je place la tête et le manche du balai dans les endroits appropriés sur l'harmoniseur et je rabaisse sa calandèle. Un vacarme assourdissant tandis que le manche se rapproche de la tête si énérgétique du balai. Le sol tremble. Une lumière intense est émise. \nPuis, plus rien. Le balai est formé et stable maintenant. Il est encore chaud. L'harmoniseur plonge le balai dans son bac d'eau. Cela produit plein de fumée.\n\n*bip, bip, biiiiip*\\tootSeparator Ça y est, mon balai est près ! Je le prends avec moi.")
                            game.hero.var["Introduction.harmoniseurUsed"] = True
                            game.hero.addItem(Item.get("balai"))
                            game.hero.addItem(Item.get("mancheBalai"), -1)
                            game.hero.addItem(Item.get("têteBalai"), -1)
                        else:
                            game.publish("Avant de pouvoir utiliser l'harmoniseur, il me faut aussi le manche du balai.")
                    else:
                        if(game.hero.var["Introduction.ManchePris"]):
                            game.publish("Avant de pouvoir utiliser l'harmoniseur, il me faut aussi la tête du balai.")
                        else:
                            game.publish("Avant de pouvoir utiliser l'harmoniseur, il me faut aussi le manche et la tête d'un balai afin de les assembler.")

        elif(args[0] == frigo[0]):
            game.publish("Je n'ai rien à mettre dans la froidimension.")

        elif(args[0] == toilettes[0]):
            game.publish("C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente.")
        else:
            game.publish("Je ne sais pas trop comment l'utiliser.")

    elif(command == Consts.actionLeave):
        if(game.hero.var["Introduction.harmoniseurUsed"]):
            if(game.hero.var["Introduction.tootPris"]):
                game.publish("Donc... \n\nJ'ai mon balai... \nJ'ai le toot à délivrer... \nToujours personne... \n\n Plus qu'à y aller !!")
                game.hero.teleport("Outside")
            else:
                game.publish("J'ai mon balai... Je pourrais délivrer le toot. Il faut que je le prenne !")
        else:
            game.publish("Je ne vais quand même pas sortir sans balai !")


    elif(command == Consts.actionWear):
        if(args[0] == lunettes[0]):
            if(not game.hero.var["Introduction.lunettesPort"]):
                game.hero.var["Introduction.lunettesPort"] = True
                game.publish("Lunettes : mises !")
            else:
                game.publish("C'est bon, j'ai déjà mis mes lunettes.")
        else:
            game.notUnderstood()

    elif(command == Consts.actionEat):
        if(args[0] == cupcake[0]):
            cupcakeItem = Item.get("cupcake")
            if(game.hero.getItemQty(cupcakeItem) > 0):
                game.publish("Quel doux bonheur...  *miam*")
                game.hero.addItem(cupcakeItem, -1)
            else:
                game.publish("Je n'ai pas de cupcake à manger... malheureusement.")
        else:
            game.publish("Je ne vais pas manger ça.")

    elif(command == Consts.actionDrink):
        if(args[0] == bouteille[0]):
            bouteilleItem = Item.get("bouteille")
            if(game.hero.getItemQty(bouteilleItem) > 0):
                game.publish("Ça désaltère !")
                game.hero.addItem(bouteilleItem, -1)
            else:
                game.publish("Je n'ai pas de bouteille à boire... malheureusement.")
        else:
            game.publish("Je ne vais pas boire ça.")

    elif(command == "faire"):
        if(args[0] == pipicaca[0]):
            game.publish("C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente.")
        elif(args[0] == sieste[0]):
            game.publish("Pas le temps, on se reposera plus tard, Marguerite.")
        else:
            game.notUnderstood()

    elif(command == Consts.actionPiss):
        game.publish("C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente.")

    elif(command == Consts.actionPoop):
        game.publish("C'est vrai, il vaut mieux passer aux toilettes avant les livraisons. On n'est jamais trop prudente.")

    elif(command == Consts.actionDance):
        game.publish("*entame un tango endiablé avec soi-même*")


    elif(command == Consts.actionSleep):
        game.publish("Pas le temps, on se reposera plus tard, Marguerite.")

    elif(command == "assembler"):
        if(args[0] == balai[0]):
            if(game.hero.var["Introduction.harmoniseurUsed"]):
                game.publish("J'ai déjà assemblé mon balai.")
            else:
                game.publish("Il me faut utiliser l'harmoniseur pour ça.")
        else:
            game.notUnderstood()

    elif(command == "monter"):
        if(args[0] == balai[0]):
            if(game.hero.var["Introduction.harmoniseurUsed"]):
                game.publish("Mmmh... Avant de monter sur mon balai je devrais surement sortir, ça éviterait des accidents.")
            elif(game.hero.var["Introduction.ArmoireOuverte"]):
                game.publish("Avant de pouvoir monter sur mon balai je devrais l'assembler.")
            else:
                game.publish("Mmmh... Il faudrait que je me trouve un balai.")
        else:
            game.notUnderstood()

    elif(command == Consts.actionOpen):
        if(args[0] == coffre[0]):
            if(game.hero.var["Introduction.CoffreOuvert"]):
                game.publish("J'ai déjà ouvert le coffre à toots.")
            else:
                game.hero.var["Introduction.CoffreOuvert"] = True
                game.publish("J'ouvre le coffre à toots pour voir son contenu et...\n\nBizarre,il n'y en a qu'un. Je devrais le prendre.")

        elif(args[0] == porte[0]):
            game.publish("Elle est déjà ouverte. Il fait beau.")

        elif(args[0] == armoire[0]):
            if(game.hero.var["Introduction.ArmoireOuverte"]):
                game.publish("J'ai déjà ouvert l'armoire.")
            else:
                game.hero.var["Introduction.ArmoireOuverte"] = True
                game.publish("J'ouvre l'armoire. Dedans il y a la tête et le manche d'un balai à assembler.")

        elif(args[0] == frigo[0]):
            game.publish("Il n'y a pas vraiment besoin d'ouvrir et de fermer la froidimension.")
    elif(command == Consts.actionPat):
        game.publish("Pandabot ronronne sous les caresses.")
