import Consts
import Log

def loadCommands():
    return [["furry","1"], ["ananas", "🍍", "2"], ["admin", "sys", "blague", "blagues", "3"], ["awoo", "Awoo", "4"], ["patriarcat", "marre", "5"],
     ["linux", "marche", "6"], ["crepe", "crêpe", "ferai", "7"], ["8", "dit"], ["chocolatine"], ["pain"]]


def loadParams():
    chocolatine = ["chocolatine"]
    pain = ["pain"]
    chocolat = ["chocolat"]
    args81 = [chocolatine, pain]
    args82 = [chocolat]
    args101 = [chocolat]
    return [ [], [], [], [], [] ,[], [], [ args81, args82 ], [], [args101]     ]


def atEnter(game):
    text = game.retrieveText("Locations/NewGame_0/welcome.txt")
    game.bot.publish(text)
    text = game.retrieveText("Locations/NewGame_0/tootChoice.txt")
    game.bot.publish(text)

def run(game, command, args):
    if(command == "furry"):
        game.hero.var["tootToDeliver"] = "Furry"
        game.hero.teleport("Introduction")
        return

    elif(command == "ananas"):
        game.hero.var["tootToDeliver"] = "Ananas"
        game.hero.teleport("Introduction")
        return

    elif(command == "admin"):
        game.hero.var["tootToDeliver"] = "adminSys"
        game.hero.teleport("Introduction")
        return

    elif(command == "awoo"):
        game.hero.var["tootToDeliver"] = "Awoo"
        game.hero.teleport("Introduction")
        return

    elif(command == "patriarcat"):
        game.hero.var["tootToDeliver"] = "BonSens"
        game.hero.teleport("Introduction")
        return

    elif(command == "linux"):
        game.hero.var["tootToDeliver"] = "passalinux"
        game.hero.teleport("Introduction")
        return

    elif(command == "crepe"):
        game.hero.var["tootToDeliver"] = "Breizh"
        game.hero.teleport("Introduction")
        return

    elif(command == "8"):
        if(args[0] == "pain" and args[1] == "chocolat"):
            game.hero.var["tootToDeliver"] = "PainAuChocolat"
            game.hero.teleport("Introduction")
        else:
            game.bot.publish("Hum... Je pense que tu as voulu dire pain au chocolat.")
            text = game.retrieveText("Locations/NewGame_0/tootChoice.txt")
            game.bot.publish(text)
        return

    elif(command == "chocolatine"):
        game.bot.publish("Hum... Je pense que tu as voulu dire pain au chocolat.")
        text = game.retrieveText("Locations/NewGame_0/tootChoice.txt")
        game.bot.publish(text)
        return

    elif(command == "pain"):
        if(args[0] == "chocolat"):
            game.hero.var["tootToDeliver"] = "PainAuChocolat"
            game.hero.teleport("Introduction")
            return
        else:
            game.bot.publish("Hum... Je pense que tu as voulu dire pain au chocolat.")
            text = game.retrieveText("Locations/NewGame_0/tootChoice.txt")
            game.bot.publish(text)
            return


    game.notUnderstood()
