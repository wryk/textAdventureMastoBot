import Consts

def loadCommands():
    return [Consts.CommandHelp, Consts.CommandLast, Consts.CommandNewGame, Consts.CommandInventory]


def loadParams():
    return [[],[], [], [Consts.everythingIsTheParameter]]



def run(game, command, args):
    if(command == Consts.CommandHelp[0]):
        game.printHelp()
        return False
    elif(command == Consts.CommandLast[0]):
        game.printLast()
        return False
    elif(command == Consts.CommandNewGame[0]):
        game.createNewGame()
        return False
    elif(command == Consts.CommandInventory[0]):
        itemToSee = None
        for key, item in game.hero.items.items():
            if item[0].name in args[0] :
                itemToSee = item[0]
                break
        game.printInventory(itemToSee)
        return False

    return True
