
class Item:
    def __init__(self, internalName, name, shortDesc, longDesc=""):
        self.internalName = internalName
        self.name = name
        self.shortDesc = shortDesc
        self.longDesc = longDesc

    def get(internalName):
        if(internalName == "tootToDeliver"):
            return Item("tootToDeliver", "toot", "Le toot que je dois délivrer.", "Le toot que je dois délivrer.")
        elif(internalName == "lunettes"):
            return Item("lunettes", "lunettes", "Lunettes de protection.", "Mes lunettes de protection pour l'harmoniseur ou pour éviter d'avoir des moucherons dans les yeux en vol.")
        elif(internalName == "mancheBalai"):
            return Item("mancheBalai", "manche", "Le manche du balai.", "Le manche du balai que je vais utiliser. Il faut que je l'assemble à la tête du balai.")
        elif(internalName == "têteBalai"):
            return Item("têteBalai", "tête", "La tête du balai.", "La tête du balai que je vais utiliser. Il faut que je l'assemble au manche du balai.")
        elif(internalName == "cupcake"):
            return Item("cupcake", "cupcake", "Un cupcake au chocolat.", "Un délicieux cupcake au chocolat fait par Ephalba.")
        elif(internalName == "bouteille"):
            return Item("bouteille", "bouteille", "Une bouteille d'eau.", "Une bouteille d'eau pour rester hydratée toute la journée.")
        elif(internalName == "balai"):
            return Item("balai", "balai", "Mon balai volant.", "Mon balai volant, il est beau comme tout.\n\nC'est pas n'importe quel balai \\var[prenom], c'est un Cleanus 2000 !")
