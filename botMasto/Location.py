from runpy import run_path
from Item import Item
import Consts

class Location(object):
    def __init__(self, name, game, globalScripts = Consts.gameGlobalScript):
        self.name = name
        self.game = game
        self.script = run_path(Consts.locationsDirectory + self.name + ".py")
        self.globalScriptsName = globalScripts
        if(len(globalScripts) > 0):
            self.globalScripts = [run_path(Consts.globalScriptsDirectory + n + ".py") for n in globalScripts]
        else:
            self.globalScripts = []

        self.possibleCommands = self.script['loadCommands']()
        self.possibleParams = self.script['loadParams']()
        for globalScript in self.globalScripts:
            pc = globalScript['loadCommands']()
            pp = globalScript['loadParams']()
            self.possibleCommands += pc
            self.possibleParams += pp


    def atEnter(self):
        self.script['atEnter'](self.game)

    def run(self, commandFound, paramsFound):
        continueRun = True
        for globalScript in self.globalScripts:
            continueRun = globalScript['run'](self.game, commandFound, paramsFound)
            if(not continueRun):
                break
        if(continueRun):
            self.script['run'](self.game, commandFound, paramsFound)
