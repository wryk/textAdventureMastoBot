debug = False
debugOutput = "data/debugOutput.txt"
debugInput = "data/debugInput.txt"
debugStatus = "data/statusDebug"
appName = "myTextAdventureApp"



appCred = "data/appCred.dat"
accountCred = "data/accountCred.dat"
apiBaseURLFile = "data/apiBaseURL.dat"
botNameFile = "data/botName.dat"
emergencyContactFile = "data/emergencyContact.dat"
proposeEmergencyContact = True
emergencyMessage = "Bonjour !\nJ'ai rencontré une erreur tout récemment alors je voulais juste te prévenir pour que tu puisses regarder d'où ça vient.\nBon courage !"
tootSeparator = '\\tootSeparator'
bddDirectory = "data/bdd/"
mediaDirectory = bddDirectory + "media/"
textsDirectory = bddDirectory + "texts/"
heroesDirectory = bddDirectory + "heroes/"
fontsDirectory = bddDirectory + "fonts/"
locationsDirectory = bddDirectory + "locations/"
globalScriptsDirectory = bddDirectory + "globalScripts/"
verboseBase = 0
verboseError = 10
helpFile = "help.txt"
gameGlobalScript = ["commonCommands"]
firstScene = "NewGame_0"
nameMaxLength = 32
tootLength = 500
notUnderstood = ["Je n'ai pas l'esprit très clair... Je devrais me concentrer sur ce que je veux faire.",
 "Je pense que je suis confus. Je ne sais pas exactement ce que je veux faire.",
"Mmmh... Je ne comprends pas moi-même ce que je souhaite.",
"Je devrais peut-être exprimer plus clairement mes idées."]

inventoryText = "Dans ma besace se trouve :\n"
tootToDeliverBase = "Furry"
log = "data/log/log.txt"

sinceID = "data/log/since_id.txt"

everythingIsTheParameter = "*"


actionLookCmd = ["regarder", "examiner", "voir", "inspecter"]
actionLook = actionLookCmd[0]

actionListenCmd = ["écouter", "ecouter", "entendre", "tendre l'oreille"]
actionListen = actionListenCmd[0]

actionTakeCmd = ["prendre", "tenir", "attraper", "soulever", "ramasser"]
actionTake = actionTakeCmd[0]

actionGoCmd = ["aller", "se diriger", "marcher"]
actionGo = actionGoCmd[0]

actionLeaveCmd = ["partir", "sortir"]
actionLeave = actionLeaveCmd[0]

actionUseCmd = ["utiliser", "actionner", "tirer"]
actionUse = actionUseCmd[0]

actionWaitCmd = ["attendre", "patienter"]
actionWait = actionWaitCmd[0]

actionWearCmd = ["mettre", "habiller", "enfiler", "porter"]
actionWear = actionWearCmd[0]

actionEatCmd = ["manger", "croquer", "dévorer", "devorer", "déguster", "deguster", "engloutir", "sustenter"]
actionEat = actionEatCmd[0]

actionDrinkCmd = ["boire", "désalterer", "desalterer"]
actionDrink = actionDrinkCmd[0]

actionMakeCmd = ["faire", "créer", "construire", "assembler"]
actionMake = actionMakeCmd[0]

actionOpenCmd = ["ouvrir"]
actionOpen = actionOpenCmd[0]

actionPissCmd = ["uriner, pisser"]
actionPiss = actionPissCmd[0]

actionPoopCmd = ["chier", "déféquer"]
actionPoop = actionPoopCmd[0]

actionDanceCmd = ["danser"]
actionDance = actionDanceCmd[0]

actionSleepCmd = ["dormir", "reposer"]
actionSleep = actionSleepCmd[0]

actionPatCmd = ["headpat", "patpat", ":blobpats:", ":patcat:"]
actionPat = actionPatCmd[0]

CommandNewGame = ["newgame"]
CommandHelp = ["help"]
CommandLast = ["last"]
CommandInventory = ["inventory"]


inviteToHelp = "La commande pour chercher de l'aide est \"" + CommandHelp[0] + "\"."
helpNew = "Bonjour ! Ceci est un bot de jeu ! Une aventure textuelle plus précisément. Pour commencer, dit \"" + CommandNewGame[0] + "\""

gameInstanceName = "witches.camp"
gamePrenom = "Marguerite"
gameNom = "Tiste"
