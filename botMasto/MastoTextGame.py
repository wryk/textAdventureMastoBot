from GeneralGame import GeneralGame
import os
import random

from PIL import Image, ImageFont, ImageDraw
from urllib.request import urlretrieve

from Hero import Hero
from Location import Location
from Item import Item
import Consts

class MastoTextGame(GeneralGame):
    def createNewGame(self):
        if(self.hero is not None):
            self.hero.reset()

    def printHelp(self):
        if(self.hero is not None):
            text = self.retrieveText(Consts.helpFile)
            self.bot.publish(text, False)

    def printHelpNew(self):
        if(self.hero is not None):
            self.bot.publish(Consts.helpNew, False)

    def printLast(self):
        if(self.hero is not None):
            self.bot.publish(self.hero.lastAnswer,  False)

    def printInventory(self, item):
        if(item is not None):
            text = item.name + "\n\n" + item.longDesc
            if(item.name == "toot"):
                self.createTootMedia()
                text += "\\media[tootToDeliver/" + self.hero.getVar("tootToDeliver") + "Custom.png]"
            self.bot.publish(self.customizeText(text), False)
        else:# it means that we want to see the whole printInventory
            text =  Consts.inventoryText
            for key, value in self.hero.items.items():
                text += "- " + value[0].name + " (" + str(value[1]) + ")\n"
            self.bot.publish(self.customizeText(text),False)


    def customizeText(self, text):
        if(self.hero is None):
            return text
        else:
            customText = text

            words = customText.split("\\")  #we separate  by \ in text
            for word in words:
                if (word[:4] == "var["):
                    index = word.find("]")
                    varName = word[4:index]  #extraction of name in \var[name]
                    customText = customText.replace("\\" + word[:index+1],str(self.hero.getVar(varName)))

            if("\\tootToDeliver" in customText):
                self.createTootMedia()
                customText = customText.replace("\\tootToDeliver", "\\media[tootToDeliver/" + self.hero.getVar("tootToDeliver") + "Custom.png]")

            return customText


    def associateUserToHero(self, status):
        account = status['account']
        accountName = account['acct']
        filename = Consts.heroesDirectory + accountName + ".txt"
        if(os.path.exists(filename)):
            locationName, locationGS, lastAnswer, var, items = self.load_object(filename)
            self.hero = Hero(self, status, locationName, locationGS, lastAnswer, var, items)
        else:
            self.hero = Hero(self, status, Consts.firstScene, Consts.gameGlobalScript, "", {}, {})
            self.hero.lastAnswer = self.retrieveText(Consts.helpFile)



    def createTootMedia(self):
        #load user's toot to deliver as a base.
        tootToDeliver = Consts.mediaDirectory + "tootToDeliver/" + self.hero.getVar("tootToDeliver") + ".png"
        base = Image.open(tootToDeliver)

        #load user's avatar
        avatarFilename, _ = urlretrieve(self.hero.lastStatus["account"]["avatar_static"])
        avatar = Image.open(avatarFilename)
        avatar = avatar.resize((52,52),Image.NEAREST)

        #Now we'll paste the avatar on the toot to deliver.
        maskAvatar = None #Eventual mask for the avatar if it has transparency.
        if(len(avatar.split()) == 4): #if transparency
            r, g, b, a = avatar.split()
            avatar = Image.merge("RGB", (r, g, b))
            maskAvatar = Image.merge("L", (a,))

        base.paste(avatar,(12,11), mask=maskAvatar)

        #The avatar is a square image, whereas in mastodon, usually avatar are in a rounded square. We apply a "filter" to round the boundaries using transparency.
        roundFilter = Image.open(Consts.mediaDirectory + "tootToDeliver/roundFilter.png")
        r, g, b, a = roundFilter.split()
        roundFilter = Image.merge("RGB", (r, g, b))
        mask = Image.merge("L", (a,))

        base.paste(roundFilter,(12,11),mask=mask)
        #Now we have a beautiful avatar on our toot. Let's write the user's name on it.

        font1 = ImageFont.truetype(Consts.fontsDirectory + "Roboto-Medium.ttf", 16)
        font2 = ImageFont.truetype(Consts.fontsDirectory + "Roboto-Regular.ttf", 15)

        drawBase = ImageDraw.Draw(base) #We consider the base image via drawBase to a draw image in order to write text on it.

        name = self.hero.lastStatus["account"]["username"]
        while(75 + drawBase.textsize(name, font=font1)[0] > 385):
            name = name[:-4] + "..."
        drawBase.text((75, 8), name, font=font1, fill="white")
        secondTextPosx = 75 + drawBase.textsize(name, font=font1)[0]+4
        while(secondTextPosx+ drawBase.textsize(' @' + name, font=font2)[0] > 385 and len(name) > 4):
            name = name[:-4] + "..."
        if(len(name) > 4):
            drawBase.text((secondTextPosx, 9), ' @' + name , font=font2, fill="#5e6781")
        base.save(Consts.mediaDirectory + "tootToDeliver/" + self.hero.getVar("tootToDeliver") + "Custom.png")
