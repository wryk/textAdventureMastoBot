from html.parser import HTMLParser

class TootHTMLParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.txt = ""
    def handle_data(self, data):
        self.txt += str(data)
